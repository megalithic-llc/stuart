.PHONY: lint
lint:
	luacheck --codes src spec spec-redis

.PHONY: check
check:
	busted -v --defer-print
